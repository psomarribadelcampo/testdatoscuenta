//
//  General.swift
//  TestMovimientos
//
//  Created by Pablo Somarriba del Campo on 31/01/2019.
//  Copyright © 2019 Pablo Somarriba del Campo. All rights reserved.
//

import Foundation
import UIKit

extension NSLayoutConstraint {
    
    //Depurar constraint
    override open var description: String {
        let id = identifier ?? "id1"
        return "id: \(id), constant: \(constant)" //you may print whatever you want here
    }
}
