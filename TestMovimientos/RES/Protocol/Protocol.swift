//
//  Protocol.swift
//  TestMovimientos
//
//  Created by Pablo Somarriba del Campo on 03/02/2019.
//  Copyright © 2019 Pablo Somarriba del Campo. All rights reserved.
//

import Foundation
import UIKit

//Al regresar a la pantalla de home, muestro el importe del último movimiento.
protocol ClassInitHomeDelegate: class {
    
    func initComponentsHome( _ ultMovimiento: String)
    
}
