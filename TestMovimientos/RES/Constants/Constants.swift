//
//  Constants.swift
//  TestMovimientos
//
//  Created by Pablo Somarriba del Campo on 31/01/2019.
//  Copyright © 2019 Pablo Somarriba del Campo. All rights reserved.
//

import Foundation

let home = "HOME"
let ok = "OK"
let noInternet = "No Internet connection"
let OtherError = "Something went wrong"
let titMovimientos = "Último movimiento"
let restoMovimientos = "Resto de movimientos"
let euro = " €"
let id = "Id: "
