//
//  CustomError.swift
//  TestMovimientos
//
//  Created by Pablo Somarriba del Campo on 31/01/2019.
//  Copyright © 2019 Pablo Somarriba del Campo. All rights reserved.
//

import Foundation

struct CustomError: Error, Decodable {
    var message: String
}
