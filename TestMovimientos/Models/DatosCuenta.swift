//
//  DatosCuenta.swift
//  TestMovimientos
//
//  Created by Pablo Somarriba del Campo on 31/01/2019.
//  Copyright © 2019 Pablo Somarriba del Campo. All rights reserved.
//

import Foundation
import UIKit

struct DatosCuenta: Decodable {

    var id: Int!
    var date: String!
    var dateValid: Date!
    var amount: Double!
    var fee: Double!
    var description:String!
}



struct DatosCuentaResponse: Decodable {
    var datCuentas: [DatosCuenta]
}

extension DatosCuenta {
    init?(json: JSON) {
        
        //Valido y elimino las fechas no validas. Solo inicializo con resgistro de fecha
        //que cumplen formato.
        func validarFecha(dateString: String) -> Date? {
            let dateFormatter = DateFormatter()
            dateFormatter.calendar = Calendar.current
            dateFormatter.locale = Locale(identifier: "es_ES")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            if let date = dateFormatter.date(from: dateString) {
                return date
            } else {
                return nil
            }
            
        }
        
        if let id = json["id"] as? Int  {
            self.id = id
        }
        if let date = json["date"] as? String {
            self.date = date
            guard let dateValida = validarFecha(dateString: date) else {
                //No proceso registro
                return nil
            }
            self.dateValid = dateValida
        }
        
        if let amount = json["amount"] as? Double {
            self.amount = amount
        }
        if let fee = json["fee"] as? Double {
            self.fee = fee
        }
        if let description = json["description"] as? String {
            self.description = description
        }
        
    }
}
