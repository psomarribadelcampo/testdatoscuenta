//
//  MovimientosTableViewCell.swift
//  TestMovimientos
//
//  Created by cifra on 1/2/19.
//  Copyright © 2019 Pablo Somarriba del Campo. All rights reserved.
//

import UIKit

class MovimientosTableViewCell: UITableViewCell {

    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var descpt: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
