//
//  HomeViewController.swift
//  TestMovimientos
//
//  Created by Pablo Somarriba del Campo on 31/01/2019.
//  Copyright © 2019 Pablo Somarriba del Campo. All rights reserved.
//
//  VISTA HOME - HomeViewController:
//      - PANTALLA INICIAL -> BUTTON "INICIA PARTE API - OBTENER MOVIMIENTOS"
//      - MODELS: DATOSCUENTA
//      - AL INICIAR LA CARGA DE DATOS SE VALIDAN LAS FECHAS.(INIT -> DATOSCUENTA)
//      - DATOS SOLO CARGADOS CON FECHAS VALIDAS -> Fechas tipo String y Date.
//      - IMPLEMENTADO SEGUE PARA PASAR LOS DATOS DE A -> B
//      - IMPLEMENTO PROCOLO ClassInitHomeDelegate PARA MOSTRAR EN HOME EL ULTIMO MOVIMIENTO.
//      - ESTO ULTIMO PERMITE PASAR INFORMACION DE B -> A

import UIKit

class HomeViewController: UIViewController, ClassInitHomeDelegate {

    func initComponentsHome(_ ultMovimiento: String) {
        self.info.text = titMovimientos + ": " + ultMovimiento
        self.loadMov.setTitle("Volver a cargar movimentos", for: .normal)
    }

    
    @IBOutlet weak var loadMov: UIButton!
    @IBOutlet weak var info: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var go: UIButton!
    
    var listMovimientos: [DatosCuenta] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = home
        
        self.spinner.isHidden = true
        self.info.isHidden = true
        self.go.isHidden = true

        
    }

    override func viewDidAppear(_ animated: Bool) {
        self.spinner.isHidden = true
        //self.info.isHidden = true
        self.go.isHidden = true
        self.loadMov.isHidden = false
    }
    
    func loadMovimentos() {
        var movsTask: URLSessionDataTask!
        
        movsTask?.cancel() //Cancel previous loading task.
        
        self.loadMov.isHidden = true
        self.spinner.isHidden = false
        self.spinner.startAnimating()
        
        movsTask = DatosCuentaService().loadDatosCuenta(forUser: self.listMovimientos) {[weak self] dataMov, error in
            DispatchQueue.main.async {
                
                if let error = error {
                    print(error.localizedDescription) //Handle service error
                    self?.info.text = error.localizedDescription
                } else if let dataMovAux = dataMov {
                    print("Movimiento OK")
                    self?.listMovimientos = dataMovAux
                    self?.stop()

                }
                
            }
        }
        
    }
    
    func stop() {

        self.spinner.stopAnimating()
        self.spinner.isHidden = true
        self.info.isHidden = false
        self.go.isHidden = false
        self.info.text = ok

    }
    
    @IBAction func clickMovimientos(_ sender: UIButton) {
        
        self.loadMovimentos()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: home, style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        
        if segue.identifier == "showmov" {
            let upcoming: MostrarMovimentosViewController = segue.destination as! MostrarMovimentosViewController
            upcoming.listMovimientosTable = self.listMovimientos
        }
        
        if let vc = segue.destination as? MostrarMovimentosViewController {
            vc.delegate = self
        }

        
        
    }
}
