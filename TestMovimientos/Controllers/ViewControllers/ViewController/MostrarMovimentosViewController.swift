//
//  MostrarMovimentosViewController.swift
//  TestMovimientos
//
//  Created by Pablo Somarriba del Campo on 31/01/2019.
//  Copyright © 2019 Pablo Somarriba del Campo. All rights reserved.
//
//  VISTA MOSTRAR MOVIMIENTOS - MOSTRARMOVIMIENTOSVIEWCONTROLLER
//        - RECIBO LA LISTA DE MOVIMIENTOS -> listMovimientosTable
//        - LA LISTA YA TIENE LA FECHA TIPO DATE() VALIDADAS.
//        - ELIMINO LOS IDS REPETIDOS VOLCANDO LOS RESULTADOS -> listaMovimientosTableValidada
//        - ORDENO DE MAS RECIENTE A MENOS, POR DATE() -> listaMovimientosTableValidada
//        - MUESTRO LA INFORMACIÓN EN DOS VISTAS:
//               - VIEW -> LABELS CON LA INFO DEL ULTIMO MOVIMIENTO
//               - TABLEVIEW -> CELL CON CADA MOVIMIENTO RESTANTE
//               - CALCULO EN IMPORTE, Y FORMATEO COLOR EN CADA "CELL"
//               - AJUSTE SIMPLE DE CADA CELL EN FUNCION DE CAMPO DESCRIPCION VACIO-NULO O CON DATOS

import UIKit

class MostrarMovimentosViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    var listMovimientosTable: [DatosCuenta] = []
    var listaMovimientosTableValidada: [DatosCuenta] = []
    
    @IBOutlet weak var tableViewMovimientos: UITableView!
    @IBOutlet weak var idMov: UILabel!
    @IBOutlet weak var dateMov: UILabel!
    @IBOutlet weak var amountMov: UILabel!
    @IBOutlet weak var descMov: UILabel!

    weak var delegate: ClassInitHomeDelegate?


    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationItem.title = titMovimientos

        self.elimnarIdsRepetidos()
        self.ordenarMovimientos()
        self.mostrarMovimientos()

        delegate?.initComponentsHome(self.amountMov.text!)
    }

    func mostrarMovimientos() {

        //Ultimo movimiento
        self.idMov.text = String(listaMovimientosTableValidada[0].id)
        self.dateMov.text = self.formateoFecha(fecha: listaMovimientosTableValidada[0].dateValid)
        
        var saldo: Double!
        if let amountAux = listaMovimientosTableValidada[0].amount, let feeAux = listaMovimientosTableValidada[0].fee {
            saldo = self.calcularSaldo(amount: amountAux, fee: feeAux)
            self.amountMov.text = String(saldo) + euro

        } else  {
            if let saldo = listaMovimientosTableValidada[0].amount {
                self.amountMov.text = String(saldo) + euro
            }
        }
        if let saldo = saldo {
            if saldo >= 0 {
                self.amountMov.textColor = UIColor.green
            } else {
                self.amountMov.textColor = UIColor.red
            }
        }

        self.descMov.text = listaMovimientosTableValidada[0].description

        //Una vez ordenada la lista, el primer registro es último movimiento.
        //Lo elimino para que no aparezca en resto de movimientos.
        self.listaMovimientosTableValidada.remove(at: 0)
        self.tableViewMovimientos.reloadData()
        
    }
    
    //Elimino los id repetidos, quedandome el id más reciente.
    func elimnarIdsRepetidos() {
        
        for regId in listMovimientosTable {
            var repetidos: Int = 0
            var dateUlt: Date!
            var regSave : DatosCuenta!
            //Impido volver a pocesar el mismo id
            if !self.listaMovimientosTableValidada.contains(where: {$0.id == regId.id}) {
                for regList in listMovimientosTable where regId.id == regList.id {
                    if dateUlt == nil {
                        dateUlt = regList.dateValid
                        regSave = regList
                    } else {
                        if regList.dateValid > dateUlt {
                            dateUlt = regList.dateValid
                            regSave = regList
                        }
                    }
                    repetidos += 1
                }
                if repetidos == 1 {
                    self.listaMovimientosTableValidada.append(regId)
                } else {
                    self.listaMovimientosTableValidada.append(regSave)
                }
            }
        }

    }
    
    //Ordeno los movimientos de más a menos recientes.
    func ordenarMovimientos() {

        self.listaMovimientosTableValidada.sort(by: { $0.dateValid > $1.dateValid})

    }
    
    func formateoFecha(fecha : Date) -> String {

        let myLocale = Locale(identifier: "es_ES")
        let formatter = DateFormatter()
        formatter.locale = myLocale
        
        let anyoNumber = NSCalendar.current.component(.year, from: fecha)
        //let monthNumber = NSCalendar.current.component(.month, from: fecha)
        let dayNumber = NSCalendar.current.component(.day, from: fecha)
        let hourNumber = NSCalendar.current.component(.hour, from: fecha)
        let minNumber = NSCalendar.current.component(.minute, from: fecha)

        let weekdayIndex = NSCalendar.current.component(.weekday, from: fecha) - 1
        let dayName = formatter.weekdaySymbols[weekdayIndex]
        let monthIndex = NSCalendar.current.component(.month, from: fecha) - 1
        let monthName = formatter.monthSymbols[monthIndex]
        
        let fechaStr: String = dayName + ", " + String(dayNumber) + " de " + monthName + " de " + String(anyoNumber) + " " + String(hourNumber) + ":" + String(minNumber)
        
        return fechaStr
    }
    
    func calcularSaldo(amount: Double, fee: Double) -> Double {
        
        //Al importe le sumo la comisión, negativa como un cargo y positiva un abono
        let saldo: Double!
        if fee < 0  {
            saldo = amount + fee
        } else {
            saldo = amount - fee
        }
        return saldo
    }
    
    // MARK: - Table view data movimientos
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.listaMovimientosTableValidada.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return restoMovimientos
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if let descAux = self.listaMovimientosTableValidada[indexPath.row].description {
            if descAux.isEmpty {
                return 90
            } else {
                return 136
            }
        } else {
            return 90
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellmov", for: indexPath) as! MovimientosTableViewCell
        
        var campos: DatosCuenta
        
        campos = self.listaMovimientosTableValidada[indexPath.row]
        
        cell.id.text = id + String(campos.id)
        cell.date.text = self.formateoFecha(fecha: campos.dateValid)
        
        var saldo: Double!
        if let amountAux = campos.amount, let feeAux = campos.fee {
            saldo = self.calcularSaldo(amount: amountAux, fee: feeAux)
            cell.amount.text = String(saldo) + euro
        } else {
            cell.amount.text = String(campos.amount) + euro
            if let saldoAux = campos.amount {
                saldo = Double(saldoAux)
            }
        }
        cell.descpt.text = campos.description

        if saldo >= 0 {
            cell.amount.textColor = UIColor.green
        } else {
            cell.amount.textColor = UIColor.red
        }

        
        return cell
    }
    

}
